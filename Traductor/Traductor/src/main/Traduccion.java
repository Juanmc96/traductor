package main;

public class Traduccion {

	private String[] es;
	private String[] fr;
	private String[] in;
	
	public Traduccion() {
		
		es = new String []{"Hola","Adios","Perro","Mochila","Gato"};
		fr = new String []{"Salut","Adieu","Chien","Cartable","Chat"};
		in = new String []{"Hello","Bye","Dog","Backpack","Cat"};
		
	}

	public String[] getEs() {
		return es;
	}

	public String[] getFr() {
		return fr;
	}

	public String[] getIn() {
		return in;
	}
	
	
	
}
